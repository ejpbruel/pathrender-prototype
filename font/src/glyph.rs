use crate::{HorizontalMetrics, Outline};
use pathrender_geometry::Rectangle;

#[derive(Clone, Debug, PartialEq)]
pub struct Glyph {
    pub horizontal_metrics: HorizontalMetrics,
    pub bounds: Rectangle,
    pub outline: Outline,
}
