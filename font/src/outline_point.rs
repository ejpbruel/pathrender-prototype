use pathrender_geometry::{PointF32, Transform, Transformation};

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct OutlinePoint {
    pub is_on_curve: bool,
    pub point: PointF32,
}

impl Transform for OutlinePoint {
    fn transform<T>(self, t: &T) -> OutlinePoint
    where
        T: Transformation,
    {
        OutlinePoint {
            is_on_curve: self.is_on_curve,
            point: self.point.transform(t),
        }
    }

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation,
    {
        *self = self.transform(t)
    }
}
