use pathrender_font::Font;
use pathrender_geometry::{AffineTransformation, PointF32, Transform, VectorF32};
use pathrender_internal_iter::{ExtendFromInternalIterator, InternalIterator};
use pathrender_path::PathIterator;
use pathrender_renderer::Renderer;
use pathrender_trapezoidator::Trapezoidator;

#[derive(Debug)]
pub struct FontAtlas<'a> {
    font: &'a Font,
    scale: f32,
    line_height: f32,
    trapezoidator: Trapezoidator,
}

impl<'a> FontAtlas<'a> {
    pub fn new(font: &Font, point_size: f32, dpi_factor: f32) -> FontAtlas {
        let scale = (point_size * 96.0 * dpi_factor) / (72.0 * font.units_per_em);
        FontAtlas {
            font,
            scale,
            line_height: (font.ascender + font.descender + font.line_gap) * scale,
            trapezoidator: Trapezoidator::new(),
        }
    }

    pub fn render_text(&mut self, position: PointF32, text: &str, renderer: &Renderer) {
        let mut trapezoids = Vec::new();
        let mut current_position = position;
        for line in text.lines() {
            for char in line.chars() {
                let glyph_index = self.font.char_code_to_glyph_index(char as u32);
                let glyph = &self.font.glyphs[glyph_index];
                trapezoids.extend_from_internal_iter(
                    self.trapezoidator.trapezoidate(
                        glyph
                            .outline
                            .commands()
                            .map({
                                let scale = self.scale;
                                move |command| {
                                    command.transform(
                                        &AffineTransformation::identity()
                                            .translate(VectorF32::new(glyph.horizontal_metrics.left_side_bearing - glyph.bounds.p_min.x, 0.0))
                                            .uniform_scale(scale)
                                            .translate(current_position.to_vector())
                                    )
                                }
                            })
                            .linearize(1.0),
                    ),
                );
                current_position.x += glyph.horizontal_metrics.advance_width * self.scale;
            }
            current_position.x = position.x;
            current_position.y -= self.line_height;
        }
        renderer.render_trapezoid_batch(&renderer.create_trapezoid_batch(&trapezoids), None);
    }
}
