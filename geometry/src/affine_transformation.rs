use crate::{LinearTransformation, PointF32, Transform, Transformation, VectorF32};

#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(C)]
pub struct AffineTransformation {
    pub xy: LinearTransformation,
    pub z: VectorF32,
}

impl AffineTransformation {
    pub fn new(xy: LinearTransformation, z: VectorF32) -> AffineTransformation {
        AffineTransformation { xy, z }
    }

    pub fn identity() -> AffineTransformation {
        AffineTransformation::new(LinearTransformation::identity(), VectorF32::zero())
    }

    pub fn scaling(v: VectorF32) -> AffineTransformation {
        AffineTransformation::new(LinearTransformation::scaling(v), VectorF32::zero())
    }

    pub fn uniform_scaling(k: f32) -> AffineTransformation {
        AffineTransformation::new(LinearTransformation::uniform_scaling(k), VectorF32::zero())
    }

    pub fn translation(v: VectorF32) -> AffineTransformation {
        AffineTransformation::new(LinearTransformation::identity(), v)
    }

    pub fn scale(self, v: VectorF32) -> AffineTransformation {
        AffineTransformation::new(self.xy.scale(v), self.z.scale(v))
    }

    pub fn uniform_scale(self, k: f32) -> AffineTransformation {
        AffineTransformation::new(self.xy.uniform_scale(k), self.z * k)
    }

    pub fn translate(self, v: VectorF32) -> AffineTransformation {
        AffineTransformation::new(self.xy, self.z + v)
    }
}

impl Transformation for AffineTransformation {
    fn transform_point(&self, p: PointF32) -> PointF32 {
        p.transform(&self.xy) + self.z
    }

    fn transform_vector(&self, v: VectorF32) -> VectorF32 {
        v.transform(&self.xy)
    }
}
