use crate::{F32Ext, PointF32, Transform, Transformation};
use std::cmp::Ordering;

#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(C)]
pub struct LineSegment {
    pub p0: PointF32,
    pub p1: PointF32,
}

impl LineSegment {
    pub fn new(p0: PointF32, p1: PointF32) -> LineSegment {
        LineSegment { p0, p1 }
    }

    pub fn compare_to_point(self, p: PointF32) -> Option<Ordering> {
        (p - self.p0).cross(self.p1 - p).partial_cmp(&0.0)
    }

    pub fn intersect_with_vertical_line(self, x: f32) -> Option<PointF32> {
        let dx = self.p1.x - self.p0.x;
        if dx == 0.0 {
            return None;
        }
        let dx1 = x - self.p0.x;
        let dx2 = self.p1.x - x;
        Some(PointF32 {
            x,
            y: if dx1 <= dx2 {
                self.p0.y.lerp(self.p1.y, dx1 / dx)
            } else {
                self.p1.y.lerp(self.p0.y, dx2 / dx)
            },
        })
    }
}

impl Transform for LineSegment {
    fn transform<T>(self, t: &T) -> LineSegment
    where
        T: Transformation,
    {
        LineSegment::new(self.p0.transform(t), self.p1.transform(t))
    }

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation,
    {
        *self = self.transform(t);
    }
}
