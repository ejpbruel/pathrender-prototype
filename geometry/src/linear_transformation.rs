use crate::{PointF32, Transformation, VectorF32};

#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(C)]
pub struct LinearTransformation {
    pub x: VectorF32,
    pub y: VectorF32,
}

impl LinearTransformation {
    pub fn new(x: VectorF32, y: VectorF32) -> LinearTransformation {
        LinearTransformation { x, y }
    }

    pub fn identity() -> LinearTransformation {
        LinearTransformation::new(VectorF32::new(1.0, 0.0), VectorF32::new(0.0, 1.0))
    }

    pub fn scaling(v: VectorF32) -> LinearTransformation {
        LinearTransformation::new(VectorF32::new(v.x, 0.0), VectorF32::new(0.0, v.y))
    }

    pub fn uniform_scaling(k: f32) -> LinearTransformation {
        LinearTransformation::scaling(VectorF32::new(k, k))
    }

    pub fn scale(self, v: VectorF32) -> LinearTransformation {
        LinearTransformation::new(self.x * v.x, self.y * v.y)
    }

    pub fn uniform_scale(self, k: f32) -> LinearTransformation {
        LinearTransformation::new(self.x * k, self.y * k)
    }

    pub fn compose(self, other: LinearTransformation) -> LinearTransformation {
        LinearTransformation::new(
            self.transform_vector(other.x),
            self.transform_vector(other.y),
        )
    }
}

impl Transformation for LinearTransformation {
    fn transform_point(&self, p: PointF32) -> PointF32 {
        (self.x * p.x + self.y * p.y).to_point()
    }

    fn transform_vector(&self, v: VectorF32) -> VectorF32 {
        self.x * v.x + self.y * v.y
    }
}
