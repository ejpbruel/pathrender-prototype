use crate::{F32Ext, PointU32, Transform, Transformation, VectorF32};
use std::ops::{Add, AddAssign, Sub, SubAssign};

#[derive(Clone, Copy, Debug, Default, PartialEq, PartialOrd)]
#[repr(C)]
pub struct PointF32 {
    pub x: f32,
    pub y: f32,
}

impl PointF32 {
    pub fn new(x: f32, y: f32) -> PointF32 {
        PointF32 { x, y }
    }

    pub fn origin() -> PointF32 {
        PointF32::new(0.0, 0.0)
    }

    pub fn to_point_u32(self) -> PointU32 {
        PointU32::new(self.x as u32, self.y as u32)
    }

    pub fn to_vector(self) -> VectorF32 {
        VectorF32::new(self.x, self.y)
    }

    pub fn lerp(self, other: PointF32, t: f32) -> PointF32 {
        PointF32::new(self.x.lerp(other.x, t), self.y.lerp(other.y, t))
    }
}

impl AddAssign<VectorF32> for PointF32 {
    fn add_assign(&mut self, vector: VectorF32) {
        *self = *self + vector;
    }
}

impl SubAssign<VectorF32> for PointF32 {
    fn sub_assign(&mut self, vector: VectorF32) {
        *self = *self - vector;
    }
}

impl Add<VectorF32> for PointF32 {
    type Output = PointF32;

    fn add(self, v: VectorF32) -> PointF32 {
        PointF32::new(self.x + v.x, self.y + v.y)
    }
}

impl Sub for PointF32 {
    type Output = VectorF32;

    fn sub(self, other: PointF32) -> VectorF32 {
        VectorF32::new(self.x - other.x, self.y - other.y)
    }
}

impl Sub<VectorF32> for PointF32 {
    type Output = PointF32;

    fn sub(self, v: VectorF32) -> PointF32 {
        PointF32::new(self.x - v.x, self.y - v.y)
    }
}

impl Transform for PointF32 {
    fn transform<T>(self, t: &T) -> PointF32
    where
        T: Transformation,
    {
        t.transform_point(self)
    }

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation,
    {
        *self = self.transform(t);
    }
}
