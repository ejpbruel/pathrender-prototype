use crate::{PointF32, VectorU32};
use std::ops::{Add, AddAssign, Sub, SubAssign};

#[derive(Clone, Copy, Debug, Default, Hash, Eq, Ord, PartialEq, PartialOrd)]
#[repr(C)]
pub struct PointU32 {
    pub x: u32,
    pub y: u32,
}

impl PointU32 {
    pub const fn new(x: u32, y: u32) -> PointU32 {
        PointU32 { x, y }
    }

    pub fn to_point_f32(self) -> PointF32 {
        PointF32::new(self.x as f32, self.y as f32)
    }

    pub fn origin() -> PointU32 {
        PointU32::new(0, 0)
    }
}

impl AddAssign<VectorU32> for PointU32 {
    fn add_assign(&mut self, vector: VectorU32) {
        *self = *self + vector;
    }
}

impl SubAssign<VectorU32> for PointU32 {
    fn sub_assign(&mut self, vector: VectorU32) {
        *self = *self - vector;
    }
}

impl Add<VectorU32> for PointU32 {
    type Output = PointU32;

    fn add(self, v: VectorU32) -> PointU32 {
        PointU32::new(self.x + v.x, self.y + v.y)
    }
}

impl Sub for PointU32 {
    type Output = VectorU32;

    fn sub(self, other: PointU32) -> VectorU32 {
        VectorU32::new(self.x - other.x, self.y - other.y)
    }
}

impl Sub<VectorU32> for PointU32 {
    type Output = PointU32;

    fn sub(self, v: VectorU32) -> PointU32 {
        PointU32::new(self.x - v.x, self.y - v.y)
    }
}
