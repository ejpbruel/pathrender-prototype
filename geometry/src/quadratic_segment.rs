use crate::{PointF32, Transform, Transformation};
use pathrender_internal_iter::InternalIterator;

#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(C)]
pub struct QuadraticSegment {
    pub p0: PointF32,
    pub p1: PointF32,
    pub p2: PointF32,
}

impl QuadraticSegment {
    pub fn new(p0: PointF32, p1: PointF32, p2: PointF32) -> QuadraticSegment {
        QuadraticSegment { p0, p1, p2 }
    }

    pub fn is_approximately_linear(self, epsilon: f32) -> bool {
        let v1 = self.p1 - self.p0;
        (if let Some(vx) = (self.p2 - self.p0).normalize() {
            v1.cross(vx).abs()
        } else {
            v1.length()
        }) < epsilon
    }

    pub fn split(self, t: f32) -> (QuadraticSegment, QuadraticSegment) {
        let p01 = self.p0.lerp(self.p1, t);
        let p12 = self.p1.lerp(self.p2, t);
        let p012 = p01.lerp(p12, t);
        (
            QuadraticSegment::new(self.p0, p01, p012),
            QuadraticSegment::new(p012, p12, self.p2),
        )
    }

    pub fn linearize(self, epsilon: f32) -> Linearize {
        Linearize {
            segment: self,
            epsilon,
        }
    }
}

impl Transform for QuadraticSegment {
    fn transform<T>(self, t: &T) -> QuadraticSegment
    where
        T: Transformation,
    {
        QuadraticSegment::new(
            self.p0.transform(t),
            self.p1.transform(t),
            self.p2.transform(t),
        )
    }

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation,
    {
        *self = self.transform(t);
    }
}

#[derive(Clone, Copy)]
pub struct Linearize {
    segment: QuadraticSegment,
    epsilon: f32,
}

impl InternalIterator for Linearize {
    type Item = PointF32;

    fn for_each<F>(self, f: &mut F) -> bool
    where
        F: FnMut(PointF32) -> bool,
    {
        if self.segment.is_approximately_linear(self.epsilon) {
            return f(self.segment.p2);
        }
        let (segment_0, segment_1) = self.segment.split(0.5);
        if !segment_0.linearize(self.epsilon).for_each(f) {
            return false;
        }
        segment_1.linearize(self.epsilon).for_each(f)
    }
}
