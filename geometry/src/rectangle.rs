use crate::{PointF32, Transform, Transformation, VectorF32};

#[derive(Clone, Copy, Debug, Default, PartialEq)]
#[repr(C)]
pub struct Rectangle {
    pub p_min: PointF32,
    pub p_max: PointF32,
}

impl Rectangle {
    pub fn new(p_min: PointF32, p_max: PointF32) -> Rectangle {
        Rectangle { p_min, p_max }
    }

    pub fn size(self) -> VectorF32 {
        VectorF32::new(self.p_max.x - self.p_min.x, self.p_max.y - self.p_min.y)
    }
}

impl Transform for Rectangle {
    fn transform<T>(self, t: &T) -> Rectangle
    where
        T: Transformation,
    {
        Rectangle::new(self.p_min.transform(t), self.p_max.transform(t))
    }

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation,
    {
        *self = self.transform(t);
    }
}
