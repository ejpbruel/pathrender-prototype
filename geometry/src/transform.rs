use crate::Transformation;

pub trait Transform {
    fn transform<T>(self, t: &T) -> Self
    where
        T: Transformation;

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation;
}
