use crate::{PointF32, VectorF32};

pub trait Transformation {
    fn transform_point(&self, point: PointF32) -> PointF32;

    fn transform_vector(&self, vector: VectorF32) -> VectorF32;
}
