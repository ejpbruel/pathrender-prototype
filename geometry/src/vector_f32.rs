use crate::{PointF32, Transform, Transformation, VectorU32};
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};

#[derive(Clone, Copy, Debug, Default, PartialEq)]
#[repr(C)]
pub struct VectorF32 {
    pub x: f32,
    pub y: f32,
}

impl VectorF32 {
    pub fn new(x: f32, y: f32) -> VectorF32 {
        VectorF32 { x, y }
    }

    pub fn zero() -> VectorF32 {
        VectorF32::new(0.0, 0.0)
    }

    pub fn to_vector_u32(self) -> VectorU32 {
        VectorU32::new(self.x as u32, self.y as u32)
    }

    pub fn to_point(self) -> PointF32 {
        PointF32::new(self.x, self.y)
    }

    pub fn length(self) -> f32 {
        self.x.hypot(self.y)
    }

    pub fn floor(self) -> VectorF32 {
        VectorF32::new(self.x.floor(), self.y.floor())
    }

    pub fn ceil(self) -> VectorF32 {
        VectorF32::new(self.x.ceil(), self.y.ceil())
    }

    pub fn normalize(self) -> Option<VectorF32> {
        let length = self.length();
        if length == 0.0 {
            None
        } else {
            Some(self / length)
        }
    }

    pub fn dot(self, other: VectorF32) -> f32 {
        self.x * other.x + self.y * other.y
    }

    pub fn cross(self, other: VectorF32) -> f32 {
        self.x * other.y - self.y * other.x
    }

    pub fn scale(self, v: VectorF32) -> VectorF32 {
        VectorF32::new(self.x * v.x, self.y * v.y)
    }
}

impl AddAssign for VectorF32 {
    fn add_assign(&mut self, other: VectorF32) {
        *self = *self + other
    }
}

impl SubAssign for VectorF32 {
    fn sub_assign(&mut self, other: VectorF32) {
        *self = *self - other
    }
}

impl MulAssign<f32> for VectorF32 {
    fn mul_assign(&mut self, k: f32) {
        *self = *self * k
    }
}

impl DivAssign<f32> for VectorF32 {
    fn div_assign(&mut self, k: f32) {
        *self = *self / k
    }
}

impl Add for VectorF32 {
    type Output = VectorF32;

    fn add(self, other: VectorF32) -> VectorF32 {
        VectorF32::new(self.x + other.x, self.y + other.y)
    }
}

impl Sub for VectorF32 {
    type Output = VectorF32;

    fn sub(self, other: VectorF32) -> VectorF32 {
        VectorF32::new(self.x - other.x, self.y - other.y)
    }
}

impl Mul<f32> for VectorF32 {
    type Output = VectorF32;

    fn mul(self, k: f32) -> VectorF32 {
        VectorF32::new(self.x * k, self.y * k)
    }
}

impl Div<f32> for VectorF32 {
    type Output = VectorF32;

    fn div(self, k: f32) -> VectorF32 {
        VectorF32::new(self.x / k, self.y / k)
    }
}

impl Neg for VectorF32 {
    type Output = VectorF32;

    fn neg(self) -> VectorF32 {
        VectorF32::new(-self.x, -self.y)
    }
}

impl Transform for VectorF32 {
    fn transform<T>(self, t: &T) -> VectorF32
    where
        T: Transformation,
    {
        t.transform_vector(self)
    }

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation,
    {
        *self = self.transform(t);
    }
}
