use crate::VectorF32;
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Sub, SubAssign};

#[derive(Clone, Copy, Debug, Default, Hash, Eq, Ord, PartialEq, PartialOrd)]
#[repr(C)]
pub struct VectorU32 {
    pub x: u32,
    pub y: u32,
}

impl VectorU32 {
    pub const fn new(x: u32, y: u32) -> VectorU32 {
        VectorU32 { x, y }
    }

    pub fn zero() -> VectorU32 {
        VectorU32::new(0, 0)
    }

    pub fn to_vector_f32(self) -> VectorF32 {
        VectorF32::new(self.x as f32, self.y as f32)
    }

    pub fn next_power_of_two(self) -> VectorU32 {
        VectorU32::new(self.x.next_power_of_two(), self.y.next_power_of_two())
    }

    pub fn scale(self, v: VectorU32) -> VectorU32 {
        VectorU32::new(self.x * v.x, self.y * v.y)
    }
}

impl AddAssign for VectorU32 {
    fn add_assign(&mut self, other: VectorU32) {
        *self = *self + other
    }
}

impl SubAssign for VectorU32 {
    fn sub_assign(&mut self, other: VectorU32) {
        *self = *self - other
    }
}

impl MulAssign<u32> for VectorU32 {
    fn mul_assign(&mut self, k: u32) {
        *self = *self * k
    }
}

impl DivAssign<u32> for VectorU32 {
    fn div_assign(&mut self, k: u32) {
        *self = *self / k
    }
}

impl Add for VectorU32 {
    type Output = VectorU32;

    fn add(self, other: VectorU32) -> VectorU32 {
        VectorU32::new(self.x + other.x, self.y + other.y)
    }
}

impl Sub for VectorU32 {
    type Output = VectorU32;

    fn sub(self, other: VectorU32) -> VectorU32 {
        VectorU32::new(self.x - other.x, self.y - other.y)
    }
}

impl Mul<u32> for VectorU32 {
    type Output = VectorU32;

    fn mul(self, k: u32) -> VectorU32 {
        VectorU32::new(self.x * k, self.y * k)
    }
}

impl Div<u32> for VectorU32 {
    type Output = VectorU32;

    fn div(self, k: u32) -> VectorU32 {
        VectorU32::new(self.x / k, self.y / k)
    }
}
