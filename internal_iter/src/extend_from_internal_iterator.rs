use crate::{InternalIterator, IntoInternalIterator};

pub trait ExtendFromInternalIterator<T> {
    fn extend_from_internal_iter<I>(&mut self, internal_iter: I)
    where
        I: IntoInternalIterator<Item = T>;
}

impl<T> ExtendFromInternalIterator<T> for Vec<T> {
    fn extend_from_internal_iter<I>(&mut self, internal_iter: I)
    where
        I: IntoInternalIterator<Item = T>,
    {
        internal_iter.into_internal_iter().for_each(&mut |item| {
            self.push(item);
            true
        });
    }
}
