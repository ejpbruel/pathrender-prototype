use crate::{ExtendFromInternalIterator, IntoInternalIterator};

pub trait FromInternalIterator<T> {
    fn from_internal_iter<I>(internal_iter: I) -> Self
    where
        I: IntoInternalIterator<Item = T>;
}

impl<T> FromInternalIterator<T> for Vec<T> {
    fn from_internal_iter<I>(internal_iter: I) -> Self
    where
        I: IntoInternalIterator<Item = T>,
    {
        let mut vec = Vec::new();
        vec.extend_from_internal_iter(internal_iter);
        vec
    }
}
