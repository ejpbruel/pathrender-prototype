use crate::FromInternalIterator;

pub trait InternalIterator {
    type Item;

    fn for_each<F>(self, f: &mut F) -> bool
    where
        F: FnMut(Self::Item) -> bool;

    fn collect<F>(self) -> F
    where
        Self: Sized,
        F: FromInternalIterator<Self::Item>,
    {
        FromInternalIterator::from_internal_iter(self)
    }

    fn map<R, F>(self, f: F) -> Map<Self, F>
    where
        Self: Sized,
        F: FnMut(Self::Item) -> R,
    {
        Map {
            internal_iter: self,
            f,
        }
    }
}

impl<I> InternalIterator for I
where
    I: Iterator,
{
    type Item = I::Item;

    fn for_each<F>(self, f: &mut F) -> bool
    where
        F: FnMut(Self::Item) -> bool,
    {
        for item in self {
            if !f(item) {
                return false;
            }
        }
        true
    }
}

#[derive(Clone, Debug)]
pub struct Map<I, F> {
    internal_iter: I,
    f: F,
}

impl<R, I, F> InternalIterator for Map<I, F>
where
    I: InternalIterator,
    F: FnMut(I::Item) -> R,
{
    type Item = R;

    fn for_each<G>(mut self, g: &mut G) -> bool
    where
        G: FnMut(Self::Item) -> bool,
    {
        self.internal_iter.for_each({
            let f = &mut self.f;
            &mut move |item| g((f)(item))
        })
    }
}
