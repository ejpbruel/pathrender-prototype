use crate::InternalIterator;

pub trait IntoInternalIterator {
    type Item;
    type IntoInternalIter: InternalIterator<Item = Self::Item>;

    fn into_internal_iter(self) -> Self::IntoInternalIter;
}

impl<I> IntoInternalIterator for I
where
    I: InternalIterator,
{
    type Item = I::Item;
    type IntoInternalIter = I;

    fn into_internal_iter(self) -> Self::IntoInternalIter {
        self
    }
}
