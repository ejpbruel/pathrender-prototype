use pathrender_geometry::{PointF32, Transform, Transformation};

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum LinePathCommand {
    MoveTo(PointF32),
    LineTo(PointF32),
    Close,
}

impl Transform for LinePathCommand {
    fn transform<T>(self, t: &T) -> LinePathCommand
    where
        T: Transformation,
    {
        match self {
            LinePathCommand::MoveTo(p) => LinePathCommand::MoveTo(p.transform(t)),
            LinePathCommand::LineTo(p) => LinePathCommand::LineTo(p.transform(t)),
            LinePathCommand::Close => LinePathCommand::Close,
        }
    }

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation,
    {
        *self = self.transform(t);
    }
}
