use crate::LinePathCommand;
use pathrender_internal_iter::InternalIterator;

pub trait LinePathIterator: InternalIterator<Item = LinePathCommand> {}

impl<I> LinePathIterator for I where I: InternalIterator<Item = LinePathCommand> {}
