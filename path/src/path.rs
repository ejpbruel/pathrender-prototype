use crate::PathCommand;
use pathrender_geometry::{PointF32, Transform, Transformation};
use pathrender_internal_iter::{
    ExtendFromInternalIterator, FromInternalIterator, InternalIterator, IntoInternalIterator,
};
use std::iter::Cloned;
use std::slice::Iter;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Path {
    verbs: Vec<Verb>,
    points: Vec<PointF32>,
}

impl Path {
    pub fn new() -> Path {
        Path::default()
    }

    pub fn points(&self) -> &[PointF32] {
        &self.points
    }

    pub fn commands(&self) -> Commands {
        Commands {
            verbs: self.verbs.iter().cloned(),
            points: self.points.iter().cloned(),
        }
    }

    pub fn points_mut(&mut self) -> &mut [PointF32] {
        &mut self.points
    }

    pub fn move_to(&mut self, p: PointF32) {
        self.verbs.push(Verb::MoveTo);
        self.points.push(p);
    }

    pub fn line_to(&mut self, p: PointF32) {
        self.verbs.push(Verb::LineTo);
        self.points.push(p);
    }

    pub fn quadratic_to(&mut self, p1: PointF32, p: PointF32) {
        self.verbs.push(Verb::QuadraticTo);
        self.points.push(p1);
        self.points.push(p);
    }

    pub fn close(&mut self) {
        self.verbs.push(Verb::Close);
    }

    pub fn clear(&mut self) {
        self.verbs.clear();
        self.points.clear();
    }
}

impl ExtendFromInternalIterator<PathCommand> for Path {
    fn extend_from_internal_iter<I>(&mut self, internal_iter: I)
    where
        I: IntoInternalIterator<Item = PathCommand>,
    {
        internal_iter.into_internal_iter().for_each(&mut |command| {
            match command {
                PathCommand::MoveTo(p) => self.move_to(p),
                PathCommand::LineTo(p) => self.line_to(p),
                PathCommand::QuadraticTo(p1, p) => self.quadratic_to(p1, p),
                PathCommand::Close => self.close(),
            }
            true
        });
    }
}

impl FromInternalIterator<PathCommand> for Path {
    fn from_internal_iter<I>(internal_iter: I) -> Self
    where
        I: IntoInternalIterator<Item = PathCommand>,
    {
        let mut path = Path::new();
        path.extend_from_internal_iter(internal_iter);
        path
    }
}

impl Transform for Path {
    fn transform<T>(mut self, t: &T) -> Path
    where
        T: Transformation,
    {
        self.transform_mut(t);
        self
    }

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation,
    {
        for point in self.points_mut() {
            point.transform_mut(t);
        }
    }
}

#[derive(Clone, Debug)]
pub struct Commands<'a> {
    verbs: Cloned<Iter<'a, Verb>>,
    points: Cloned<Iter<'a, PointF32>>,
}

impl<'a> Iterator for Commands<'a> {
    type Item = PathCommand;

    fn next(&mut self) -> Option<PathCommand> {
        self.verbs.next().map(|verb| match verb {
            Verb::MoveTo => PathCommand::MoveTo(self.points.next().unwrap()),
            Verb::LineTo => PathCommand::LineTo(self.points.next().unwrap()),
            Verb::QuadraticTo => {
                PathCommand::QuadraticTo(self.points.next().unwrap(), self.points.next().unwrap())
            }
            Verb::Close => PathCommand::Close,
        })
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
enum Verb {
    MoveTo,
    LineTo,
    QuadraticTo,
    Close,
}
