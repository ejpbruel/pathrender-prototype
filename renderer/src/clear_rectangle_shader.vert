#version 100
precision highp float;
precision highp int;

uniform vec2 uViewportSize;

attribute vec2 aParameter;
attribute vec2 aPMin;
attribute vec2 aPMax;

void main() {
    vec2 position = mix(aPMin, aPMax, aParameter);
    gl_Position = vec4(position * 2.0 / uViewportSize - 1.0, 0.0, 1.0);
}