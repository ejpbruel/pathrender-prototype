use gl::types::{GLenum, GLint, GLsizei, GLsizeiptr, GLuint};
use pathrender_geometry::{PointF32, Rectangle, Trapezoid, VectorU32};
use std::ffi::{c_void, CStr, CString};
use std::{mem, ptr};

#[derive(Debug)]
pub struct Renderer {
    window_size: VectorU32,
    framebuffer: GLuint,
    trapezoid_program: GLuint,
    textured_rectangle_program: GLuint,
    vertex_buffer: GLuint,
    index_buffer: GLuint,
}

impl Renderer {
    pub fn new(window_size: VectorU32) -> Renderer {
        unsafe {
            let mut framebuffer = mem::uninitialized();
            gl::GenFramebuffers(1, &mut framebuffer);
            let vertex_shader =
                compile_shader(gl::VERTEX_SHADER, include_str!("trapezoid_shader.vert"));
            let fragment_shader =
                compile_shader(gl::FRAGMENT_SHADER, include_str!("trapezoid_shader.frag"));
            let trapezoid_program = link_program(vertex_shader, fragment_shader);
            gl::DeleteShader(vertex_shader);
            gl::DeleteShader(fragment_shader);
            let vertex_shader = compile_shader(
                gl::VERTEX_SHADER,
                include_str!("textured_rectangle_shader.vert"),
            );
            let fragment_shader = compile_shader(
                gl::FRAGMENT_SHADER,
                include_str!("textured_rectangle_shader.frag"),
            );
            let textured_rectangle_program = link_program(vertex_shader, fragment_shader);
            gl::DeleteShader(vertex_shader);
            gl::DeleteShader(fragment_shader);
            let vertices = &[
                Vertex {
                    parameter: [0.0, 0.0],
                },
                Vertex {
                    parameter: [1.0, 0.0],
                },
                Vertex {
                    parameter: [0.0, 1.0],
                },
                Vertex {
                    parameter: [1.0, 1.0],
                },
            ];
            let indices: &[u8] = &[0, 1, 2, 2, 1, 3];
            let mut vertex_buffer = mem::uninitialized();
            gl::GenBuffers(1, &mut vertex_buffer);
            gl::BindBuffer(gl::ARRAY_BUFFER, vertex_buffer);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (vertices.len() * mem::size_of::<Vertex>()) as GLsizeiptr,
                vertices.as_ptr() as *const _,
                gl::STATIC_DRAW,
            );
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            let mut index_buffer = mem::uninitialized();
            gl::GenBuffers(1, &mut index_buffer);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, index_buffer);
            gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                (indices.len() * mem::size_of::<u8>()) as GLsizeiptr,
                indices.as_ptr() as *const _,
                gl::STATIC_DRAW,
            );
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
            Renderer {
                window_size,
                framebuffer,
                trapezoid_program,
                textured_rectangle_program,
                vertex_buffer,
                index_buffer,
            }
        }
    }

    pub fn create_trapezoid_batch(&self, trapezoids: &[Trapezoid]) -> TrapezoidBatch {
        unsafe {
            let mut instance_buffer = mem::uninitialized();
            gl::GenBuffers(1, &mut instance_buffer);
            gl::BindBuffer(gl::ARRAY_BUFFER, instance_buffer);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (trapezoids.len() * mem::size_of::<Trapezoid>()) as GLsizeiptr,
                trapezoids.as_ptr() as *const c_void,
                gl::STATIC_DRAW,
            );
            let mut vertex_array = mem::uninitialized();
            gl::GenVertexArrays(1, &mut vertex_array);
            gl::BindVertexArray(vertex_array);
            let parameter_attribute = gl::GetAttribLocation(
                self.trapezoid_program,
                CString::new("aParameter").unwrap().as_ptr(),
            ) as GLuint;
            gl::EnableVertexAttribArray(parameter_attribute);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vertex_buffer);
            gl::VertexAttribPointer(
                parameter_attribute,
                2,
                gl::FLOAT,
                gl::FALSE,
                mem::size_of::<Vertex>() as GLsizei,
                &(*(0 as *const Vertex)).parameter as *const _ as *const c_void,
            );
            let xs_attribute = gl::GetAttribLocation(
                self.trapezoid_program,
                CString::new("aXs").unwrap().as_ptr(),
            ) as GLuint;
            gl::EnableVertexAttribArray(xs_attribute);
            gl::VertexAttribDivisor(xs_attribute, 1);
            gl::BindBuffer(gl::ARRAY_BUFFER, instance_buffer);
            gl::VertexAttribPointer(
                xs_attribute,
                2,
                gl::FLOAT,
                gl::FALSE,
                mem::size_of::<Trapezoid>() as GLsizei,
                &(*(0 as *const Trapezoid)).xs as *const _ as *const c_void,
            );
            let ys_attribute = gl::GetAttribLocation(
                self.trapezoid_program,
                CString::new("aYs").unwrap().as_ptr(),
            ) as GLuint;
            gl::EnableVertexAttribArray(ys_attribute);
            gl::VertexAttribDivisor(ys_attribute, 1);
            gl::BindBuffer(gl::ARRAY_BUFFER, instance_buffer);
            gl::VertexAttribPointer(
                ys_attribute,
                4,
                gl::FLOAT,
                gl::FALSE,
                mem::size_of::<Trapezoid>() as GLsizei,
                &(*(0 as *const Trapezoid)).ys as *const _ as *const c_void,
            );
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.index_buffer);
            gl::BindVertexArray(0);
            gl::DeleteBuffers(1, &instance_buffer);
            TrapezoidBatch {
                vertex_array,
                instance_count: trapezoids.len() as GLsizei,
            }
        }
    }

    pub fn create_textured_rectangle_batch(
        &self,
        textured_rectangles: &[TexturedRectangle],
    ) -> TexturedRectangleBatch {
        unsafe {
            let mut instance_buffer = mem::uninitialized();
            gl::GenBuffers(1, &mut instance_buffer);
            gl::BindBuffer(gl::ARRAY_BUFFER, instance_buffer);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (textured_rectangles.len() * mem::size_of::<TexturedRectangle>()) as GLsizeiptr,
                textured_rectangles.as_ptr() as *const c_void,
                gl::STATIC_DRAW,
            );
            let mut vertex_array = mem::uninitialized();
            gl::GenVertexArrays(1, &mut vertex_array);
            gl::BindVertexArray(vertex_array);
            let parameter_attribute = gl::GetAttribLocation(
                self.textured_rectangle_program,
                CString::new("aParameter").unwrap().as_ptr(),
            ) as GLuint;
            gl::EnableVertexAttribArray(parameter_attribute);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vertex_buffer);
            gl::VertexAttribPointer(
                parameter_attribute,
                2,
                gl::FLOAT,
                gl::FALSE,
                mem::size_of::<Vertex>() as GLsizei,
                &(*(0 as *const Vertex)).parameter as *const _ as *const c_void,
            );
            let pmin_attribute = gl::GetAttribLocation(
                self.textured_rectangle_program,
                CString::new("aPMin").unwrap().as_ptr(),
            ) as GLuint;
            gl::EnableVertexAttribArray(pmin_attribute);
            gl::VertexAttribDivisor(pmin_attribute, 1);
            gl::BindBuffer(gl::ARRAY_BUFFER, instance_buffer);
            gl::VertexAttribPointer(
                pmin_attribute,
                2,
                gl::FLOAT,
                gl::FALSE,
                mem::size_of::<TexturedRectangle>() as GLsizei,
                &(*(0 as *const TexturedRectangle)).rectangle.p_min as *const _ as *const c_void,
            );
            let pmax_attribute = gl::GetAttribLocation(
                self.textured_rectangle_program,
                CString::new("aPMax").unwrap().as_ptr(),
            ) as GLuint;
            gl::EnableVertexAttribArray(pmax_attribute);
            gl::VertexAttribDivisor(pmax_attribute, 1);
            gl::BindBuffer(gl::ARRAY_BUFFER, instance_buffer);
            gl::VertexAttribPointer(
                pmax_attribute,
                2,
                gl::FLOAT,
                gl::FALSE,
                mem::size_of::<TexturedRectangle>() as GLsizei,
                &(*(0 as *const TexturedRectangle)).rectangle.p_max as *const _ as *const c_void,
            );
            let tex_coord_attribute = gl::GetAttribLocation(
                self.textured_rectangle_program,
                CString::new("aTexCoord").unwrap().as_ptr(),
            ) as GLuint;
            gl::EnableVertexAttribArray(tex_coord_attribute);
            gl::VertexAttribDivisor(tex_coord_attribute, 1);
            gl::BindBuffer(gl::ARRAY_BUFFER, instance_buffer);
            gl::VertexAttribPointer(
                tex_coord_attribute,
                2,
                gl::FLOAT,
                gl::FALSE,
                mem::size_of::<TexturedRectangle>() as GLsizei,
                &(*(0 as *const TexturedRectangle)).tex_coord as *const _ as *const c_void,
            );
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.index_buffer);
            gl::BindVertexArray(0);
            gl::DeleteBuffers(1, &instance_buffer);
            TexturedRectangleBatch {
                vertex_array,
                instance_count: textured_rectangles.len() as GLsizei,
            }
        }
    }

    pub fn create_texture(&self, size: VectorU32) -> Texture {
        unsafe {
            let mut texture = mem::uninitialized();
            gl::GenTextures(1, &mut texture);
            gl::BindTexture(gl::TEXTURE_2D, texture);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as GLint);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as GLint);
            gl::TexImage2D(
                gl::TEXTURE_2D,
                0,
                gl::RGB as GLint,
                size.x as GLsizei,
                size.y as GLsizei,
                0,
                gl::RGB,
                gl::UNSIGNED_BYTE,
                ptr::null(),
            );
            gl::BindFramebuffer(gl::FRAMEBUFFER, self.framebuffer);
            gl::FramebufferTexture(gl::FRAMEBUFFER, gl::COLOR_ATTACHMENT0, texture, 0);
            assert!(gl::CheckFramebufferStatus(gl::FRAMEBUFFER) == gl::FRAMEBUFFER_COMPLETE);
            gl::ClearColor(0.0, 0.0, 0.0, 1.0);
            gl::Clear(gl::COLOR_BUFFER_BIT);
            gl::BindFramebuffer(gl::FRAMEBUFFER, 0);
            Texture { texture, size }
        }
    }

    pub fn render_trapezoid_batch(
        &self,
        trapezoid_batch: &TrapezoidBatch,
        texture: Option<&Texture>,
    ) {
        unsafe {
            let viewport_size = if let Some(texture) = texture {
                gl::BindFramebuffer(gl::FRAMEBUFFER, self.framebuffer);
                gl::FramebufferTexture(gl::FRAMEBUFFER, gl::COLOR_ATTACHMENT0, texture.texture, 0);
                assert!(gl::CheckFramebufferStatus(gl::FRAMEBUFFER) == gl::FRAMEBUFFER_COMPLETE);
                texture.size
            } else {
                self.window_size
            };
            gl::Enable(gl::BLEND);
            gl::BlendEquation(gl::FUNC_ADD);
            gl::BlendFunc(gl::ONE, gl::ONE);
            gl::Viewport(0, 0, viewport_size.x as GLsizei, viewport_size.y as GLsizei);
            gl::UseProgram(self.trapezoid_program);
            gl::Uniform2fv(
                gl::GetUniformLocation(
                    self.trapezoid_program,
                    CString::new("uViewportSize").unwrap().as_ptr(),
                ),
                1,
                [viewport_size.x as f32, viewport_size.y as f32].as_ptr(),
            );
            gl::BindVertexArray(trapezoid_batch.vertex_array);
            gl::DrawElementsInstanced(
                gl::TRIANGLES,
                6,
                gl::UNSIGNED_BYTE,
                ptr::null(),
                trapezoid_batch.instance_count,
            );
            gl::BindVertexArray(0);
            gl::UseProgram(0);
            gl::Disable(gl::BLEND);
            gl::BindFramebuffer(gl::FRAMEBUFFER, 0);
        }
    }

    pub fn render_textured_rectangle_batch(
        &self,
        texture: &Texture,
        textured_rectangle_batch: &TexturedRectangleBatch,
    ) {
        unsafe {
            gl::BindFramebuffer(gl::FRAMEBUFFER, 0);
            gl::Enable(gl::BLEND);
            gl::BlendEquation(gl::FUNC_ADD);
            gl::BlendFunc(gl::ONE, gl::ONE);
            gl::Viewport(
                0,
                0,
                self.window_size.x as GLsizei,
                self.window_size.y as GLsizei,
            );
            gl::UseProgram(self.textured_rectangle_program);
            gl::Uniform2fv(
                gl::GetUniformLocation(
                    self.textured_rectangle_program,
                    CString::new("uViewportSize").unwrap().as_ptr(),
                ),
                1,
                [self.window_size.x as f32, self.window_size.y as f32].as_ptr(),
            );
            gl::Uniform2fv(
                gl::GetUniformLocation(
                    self.textured_rectangle_program,
                    CString::new("uTextureSize").unwrap().as_ptr(),
                ),
                1,
                [texture.size.x as f32, texture.size.y as f32].as_ptr(),
            );
            gl::ActiveTexture(gl::TEXTURE0);
            gl::BindTexture(gl::TEXTURE_2D, texture.texture);
            gl::Uniform1i(
                gl::GetUniformLocation(
                    self.textured_rectangle_program,
                    CString::new("uTexture").unwrap().as_ptr(),
                ),
                0,
            );
            gl::BindVertexArray(textured_rectangle_batch.vertex_array);
            gl::DrawElementsInstanced(
                gl::TRIANGLES,
                6,
                gl::UNSIGNED_BYTE,
                ptr::null(),
                textured_rectangle_batch.instance_count,
            );
            gl::BindVertexArray(0);
            gl::UseProgram(0);
            gl::Disable(gl::BLEND);
        }
    }

    pub fn set_window_size(&mut self, window_size: VectorU32) {
        self.window_size = window_size;
    }
}

impl Drop for Renderer {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteBuffers(1, &mut self.index_buffer);
            gl::DeleteBuffers(1, &mut self.vertex_buffer);
            gl::DeleteProgram(self.trapezoid_program);
            gl::DeleteFramebuffers(1, &mut self.framebuffer);
        }
    }
}

#[derive(Debug, Eq, Hash, PartialEq)]
pub struct ClearRectangleBatch {
    vertex_array: GLuint,
    instance_count: GLsizei,
}

impl Drop for ClearRectangleBatch {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteVertexArrays(1, &self.vertex_array);
        }
    }
}

#[derive(Debug, Eq, Hash, PartialEq)]
pub struct TrapezoidBatch {
    vertex_array: GLuint,
    instance_count: GLsizei,
}

impl Drop for TrapezoidBatch {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteVertexArrays(1, &self.vertex_array);
        }
    }
}

#[derive(Debug, Eq, Hash, PartialEq)]
pub struct TexturedRectangleBatch {
    vertex_array: GLuint,
    instance_count: GLsizei,
}

impl Drop for TexturedRectangleBatch {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteVertexArrays(1, &self.vertex_array);
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(C)]
pub struct TexturedRectangle {
    pub rectangle: Rectangle,
    pub tex_coord: PointF32,
}

#[derive(Debug, Eq, Hash, PartialEq)]
pub struct Texture {
    texture: GLuint,
    size: VectorU32,
}

impl Drop for Texture {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteTextures(1, &self.texture);
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(C)]
struct Vertex {
    parameter: [f32; 2],
}

unsafe fn compile_shader(type_: GLenum, string: &str) -> GLuint {
    let shader = gl::CreateShader(type_);
    gl::ShaderSource(
        shader,
        1,
        &CString::new(string).unwrap().as_ptr(),
        ptr::null(),
    );
    gl::CompileShader(shader);
    let mut status = mem::uninitialized();
    gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut status);
    if status == 0 {
        let mut length = mem::uninitialized();
        gl::GetShaderiv(shader, gl::INFO_LOG_LENGTH, &mut length);
        let mut log = Vec::with_capacity(length as usize);
        gl::GetShaderInfoLog(shader, length, ptr::null_mut(), log.as_mut_ptr());
        log.set_len(length as usize);
        panic!(CStr::from_ptr(log.as_ptr()).to_str().unwrap());
    }
    shader
}

unsafe fn link_program(vertex_shader: GLuint, fragment_shader: GLuint) -> GLuint {
    let trapezoid_program = gl::CreateProgram();
    gl::AttachShader(trapezoid_program, vertex_shader);
    gl::AttachShader(trapezoid_program, fragment_shader);
    gl::LinkProgram(trapezoid_program);
    let mut status = mem::uninitialized();
    gl::GetProgramiv(trapezoid_program, gl::LINK_STATUS, &mut status);
    if status == 0 {
        let mut length = mem::uninitialized();
        gl::GetProgramiv(trapezoid_program, gl::INFO_LOG_LENGTH, &mut length);
        let mut log = Vec::with_capacity(length as usize);
        gl::GetProgramInfoLog(trapezoid_program, length, ptr::null_mut(), log.as_mut_ptr());
        log.set_len(length as usize);
        panic!(CStr::from_ptr(log.as_ptr()).to_str().unwrap());
    }
    trapezoid_program
}
