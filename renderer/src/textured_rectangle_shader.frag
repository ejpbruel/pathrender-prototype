#version 100
precision highp float;
precision highp int;

uniform sampler2D uTexture;

varying vec2 vTexCoord;

void main() {
    gl_FragColor = texture2D(uTexture, vTexCoord);
}