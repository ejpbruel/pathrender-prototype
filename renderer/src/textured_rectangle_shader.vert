#version 100
precision highp float;
precision highp int;

uniform vec2 uViewportSize;
uniform vec2 uTextureSize;

attribute vec2 aParameter;
attribute vec2 aPMin;
attribute vec2 aPMax;
attribute vec2 aTexCoord;

varying vec2 vTexCoord;

void main() {
    vec2 position = mix(aPMin, aPMax, aParameter);
    gl_Position = vec4(position * 2.0 / uViewportSize - 1.0, 0.0, 1.0);
    vTexCoord = mix(aTexCoord, aTexCoord + (aPMax - aPMin), aParameter) / uTextureSize;
}