#version 100
precision highp float;
precision highp int;

varying vec2 vP0;
varying vec2 vP1;
varying vec2 vP2;
varying vec2 vP3;

vec2 intersectLineSegmentWithVerticalLine(vec2 p0, vec2 p1, float x) {
    return vec2(x, mix(p0.y, p1.y, (x - p0.x) / (p1.x - p0.x)));
}

vec2 intersectLineSegmentWithHorizontalLine(vec2 p0, vec2 p1, float y) {
    return vec2(mix(p0.x, p1.x, (y - p0.y) / (p1.y - p0.y)), y);
}

float computeClampedRightTrapezoidArea(vec2 p0, vec2 p1, vec2 pMin, vec2 pMax) {
    float x0 = clamp(p0.x, pMin.x, pMax.x);
    float x1 = clamp(p1.x, pMin.x, pMax.x);
    if (p0.x < pMin.x && pMin.x < p1.x) {
        p0 = intersectLineSegmentWithVerticalLine(p0, p1, pMin.x);
    }
    if (p0.x < pMax.x && pMax.x < p1.x) {
        p1 = intersectLineSegmentWithVerticalLine(p0, p1, pMax.x);
    }
    if (p0.y < pMin.y && pMin.y < p1.y) {
        p0 = intersectLineSegmentWithHorizontalLine(p0, p1, pMin.y);
    }
    if (p1.y < pMin.y && pMin.y < p0.y) {
        p1 = intersectLineSegmentWithHorizontalLine(p1, p0, pMin.y);
    }
    if (p0.y < pMax.y && pMax.y < p1.y) {
        p1 = intersectLineSegmentWithHorizontalLine(p0, p1, pMax.y);
    }
    if (p1.y < pMax.y && pMax.y < p0.y) {
        p0 = intersectLineSegmentWithHorizontalLine(p1, p0, pMax.y);
    }
    p0 = clamp(p0, pMin, pMax);
    p1 = clamp(p1, pMin, pMax);
    float h0 = pMax.y - p0.y;
    float h1 = pMax.y - p1.y;
    float a0 = (p0.x - x0) * h0;
    float a1 = (p1.x - p0.x) * (h0 + h1) / 2.0;
    float a2 = (x1 - p1.x) * h1;
    return a0 + a1 + a2;
}

float computeClampedTrapezoidArea(vec2 pMin, vec2 pMax) {
    float a0 = computeClampedRightTrapezoidArea(vP0, vP1, pMin, pMax);
    float a1 = computeClampedRightTrapezoidArea(vP2, vP3, pMin, pMax);
    return a0 - a1;
}

void main() {
    vec2 pMin = gl_FragCoord.xy - 0.5;
    vec2 pMax = gl_FragCoord.xy + 0.5;
    float x0 = pMin.x + 1.0 / 3.0;
    float x1 = pMax.x - 1.0 / 3.0;
    gl_FragColor = vec4(
        computeClampedTrapezoidArea(pMin, vec2(x0, pMax.y)) * 3.0,
        computeClampedTrapezoidArea(vec2(x0, pMin.y), vec2(x1, pMax.y)) * 3.0,
        computeClampedTrapezoidArea(vec2(x1, pMin.y), pMax) * 3.0,
        1.0
    );
    float coverage = computeClampedTrapezoidArea(pMin, pMax);
    gl_FragColor = vec4(
        coverage,
        coverage,
        coverage,
        1.0
    );
}