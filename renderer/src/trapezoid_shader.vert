#version 100
precision highp float;
precision highp int;

uniform vec2 uViewportSize;

attribute vec2 aParameter;
attribute vec2 aXs;
attribute vec4 aYs;

varying vec2 vP0;
varying vec2 vP1;
varying vec2 vP2;
varying vec2 vP3;

void main() {
    vec2 positionMin = vec2(aXs.x, min(aYs.x, aYs.y));
    vec2 positionMax = vec2(aXs.y, max(aYs.z, aYs.w));
    vec2 position = mix(positionMin - 1.0, positionMax + 1.0, aParameter);
    gl_Position = vec4(position * 2.0 / uViewportSize - 1.0, 0.0, 1.0);
    vP0 = vec2(aXs.x, aYs.x);
    vP1 = vec2(aXs.y, aYs.y);
    vP2 = vec2(aXs.x, aYs.z);
    vP3 = vec2(aXs.y, aYs.w);
}