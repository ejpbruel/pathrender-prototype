use glutin::dpi::LogicalSize;
use glutin::{ContextBuilder, Event, EventsLoop, WindowBuilder, WindowEvent};
use pathrender_font_atlas::FontAtlas;
use pathrender_geometry::{PointF32, VectorU32};
use pathrender_renderer::Renderer;

fn main() {
    let path = std::path::Path::new(env!("CARGO_MANIFEST_DIR")).join("fonts/Ubuntu-R.ttf");
    let font = pathrender_ttf_parser::parse_ttf(&std::fs::read(path).unwrap()).unwrap();
    let mut events_loop = EventsLoop::new();
    let windowed_context = ContextBuilder::new()
        .with_vsync(true)
        .build_windowed(
            WindowBuilder::new().with_dimensions(LogicalSize::new(512.0, 512.0)),
            &events_loop,
        )
        .unwrap();
    let windowed_context = unsafe {
        let windowed_context = windowed_context.make_current().unwrap();
        gl::load_with(|symbol| windowed_context.get_proc_address(symbol) as *const _);
        gl::ClearColor(0.0, 0.0, 0.0, 1.0);
        windowed_context
    };
    let window = windowed_context.window();
    let logical_size = window.get_inner_size().unwrap();
    let dpi_factor = window.get_hidpi_factor();
    let physical_size = logical_size.to_physical(dpi_factor);
    let mut renderer = Renderer::new(VectorU32::new(
        physical_size.width as u32,
        physical_size.height as u32,
    ));
    let mut font_atlas = FontAtlas::new(&font, 12.0, dpi_factor as f32);
    let mut running = true;
    while running {
        events_loop.poll_events(|event| match event {
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::CloseRequested => running = false,
                WindowEvent::Refresh => {
                    unsafe {
                        gl::Clear(gl::COLOR_BUFFER_BIT);
                    }
                    font_atlas.render_text(PointF32::new(64.0, 512.0 - 64.0), "OoLleeeeaaaacccmmmmddddffff", &renderer);
                    windowed_context.swap_buffers().unwrap();
                }
                WindowEvent::Resized(logical_size) => {
                    let physical_size = logical_size.to_physical(dpi_factor);
                    windowed_context.resize(physical_size);
                    renderer.set_window_size(VectorU32::new(
                        physical_size.width as u32,
                        physical_size.height as u32,
                    ));
                }
                _ => (),
            },
            _ => (),
        });
    }
}
